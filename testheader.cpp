#include "testheader.h"
#include <QDebug>
#include <QMouseEvent>
#include <QTableView>
#include <QTextDocument>
#include <QTextOption>
#include <QStringList>


TestHeader::TestHeader(Qt::Orientation orientation, QWidget *parent) : QHeaderView(orientation,parent),
    isResize(false),
    headerHeight(0),
    testMin(0),
    headerWidth(0)
{
    setDefaultAlignment(Qt::AlignCenter | static_cast<Qt::Alignment>(Qt::TextWordWrap));
    setSectionsClickable(true);
    setHighlightSections(true);
    setMouseTracking(true);

    if(orientation == Qt::Horizontal) {
        connect(this,&TestHeader::sectionResized,this,&TestHeader::onResizeHeader);
    } else {
        connect(this,&TestHeader::headerResized,this,&TestHeader::onResizeHeader);
    }
}

TestHeader::~TestHeader()
{
    disconnect();
}

void TestHeader::onResizeHeader(int index,int o_size,int n_size)
{
    Q_UNUSED(index)

    if (orientation() ==  Qt::Horizontal) {
        calcSize();
        setMinimumSectionSize(headerWidth * 0.55);
        if (n_size < o_size) {
            setMinimumHeight(headerHeight);
        } else {
            setMinimumHeight(headerHeight);
        }
    } else {
        calcSize();
        if(n_size < minimumWidth()) {
            for(int i=0; i < count();i++) {
                resizeSection(i,headerHeight);
            }
        } else {
            for(int i=0; i < count();i++) {
                resizeSection(i,headerHeight/ 2);
            }
        }
    }
    headerHeight = 0;
    headerWidth = 0;
}


void TestHeader::calcSize()
{
    QTextDocument *doc = new QTextDocument();
    static QTextOption to;
    to.setWrapMode(QTextOption::WordWrap);
    doc->setDefaultTextOption(to);
    doc->setDocumentMargin(5);
    if (orientation() == Qt::Horizontal) {
        for (int i= 0 ; i < count(); i++) {
            doc->setTextWidth(sectionSize(i));
            doc->setPlainText(model()->headerData(i,Qt::Horizontal,Qt::DisplayRole).toString());
            headerHeight = qMax(headerHeight,static_cast<int>(doc->size().height()));
            headerWidth = qMax(headerWidth,static_cast<int>(doc->size().width()));
        }
    } else {
        doc->setDocumentMargin(8);
        for (int i= 0 ; i < count(); i++) {
            doc->setTextWidth(sectionSize(i));
            doc->setPlainText(model()->headerData(i,Qt::Vertical,Qt::DisplayRole).toString());
            headerWidth = qMax(headerWidth,static_cast<int>(doc->size().width()));
            headerHeight = qMax(headerHeight,static_cast<int>(doc->size().height()));
        }
    }
    delete doc;
}
void TestHeader::mousePressEvent(QMouseEvent *e)
{

    if (orientation() == Qt::Vertical) {
        if(e->pos().x() >= rect().width() - 5){
            setCursor(Qt::SplitHCursor);
            isResize = true;
        }
    } else if (orientation() == Qt::Horizontal) {
        if (e->pos().y() >= rect().height() - 5){
            setCursor(Qt::SplitVCursor);
            isResize = true;
        }
    } else {

    }
    QHeaderView::mousePressEvent(e);
}

void TestHeader::mouseReleaseEvent(QMouseEvent *e)
{
    if(isResize){
        setCursor(Qt::ArrowCursor);
        isResize=false;
    }
    QHeaderView::mouseReleaseEvent(e);
}

void TestHeader::mouseMoveEvent(QMouseEvent *e)
{
    if(isResize){
        calcSize();
        if(orientation() == Qt::Vertical) {
            if(e->pos().x() > 0 && e->pos().x() > headerWidth) {
                emit headerResized(0,0,e->pos().x());
                setFixedWidth(e->pos().x());
            }
        } else {
            if(e->pos().y() > 0  && e->pos().y() > headerHeight) {
                setFixedHeight(e->pos().y());
                emit headerResized(0,0,e->pos().y());
            }
            headerHeight = 0;
        }
    }

    QHeaderView::mouseMoveEvent(e);
}

