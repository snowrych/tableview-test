#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "testmodel.h"
#include "testheader.h"
#include <QHeaderView>
#include <QDebug>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow),
    view(Q_NULLPTR)
{
    ui->setupUi(this);
    resize(600 ,400);

    view = new QTableView();
    view->setGeometry(x(),y(),width(),height());
    setCentralWidget(view);

    TestModel *m= new TestModel;
        m->setOri(TestModel::Vertical);
    view->setModel(m);

    TestHeader *header = new TestHeader(m->ori() == TestModel::Horizontal ? Qt::Horizontal : Qt::Vertical );

    QAction *act = new QAction("Reset model");
    act->setCheckable(true);
    ui->toolBar->addAction(act);


    if(m->ori() == TestModel::Horizontal) {
        view->setHorizontalHeader(header);
        view->horizontalHeader()->setMouseTracking(true);
        connect(header,&TestHeader::headerResized, [=](int val1,int val2,int size){
            Q_UNUSED(val1)
            Q_UNUSED(val2)
//            for(int i = 0;i< view->verticalHeader()->count(); i++)
//                view->verticalHeader()->resizeSection(i, size);
//            setSectionResizeMode(0, QtWidgets.QHeaderView.Stretch)
            view->verticalHeader()->sectionResized(0,view->verticalHeader()->sectionSize(0),size);
        });

    } else if(m->ori() == TestModel::Vertical) {
        view->setVerticalHeader(header);
        view->verticalHeader()->setMouseTracking(true);
        connect(header,&TestHeader::headerResized, [=](int val1,int val2 ,int size){
            Q_UNUSED(val1)
            Q_UNUSED(val2)
            for(int i = 0;i< view->horizontalHeader()->count(); i++)
                view->horizontalHeader()->resizeSection(i, size);
//            view->verticalHeader()->resizeSections(QHeaderView::ResizeToContents);
        });

    }
//    view->setSelectionBehavior(m->ori() == TestModel::Vertical ? QAbstractItemView::SelectRows : QAbstractItemView::SelectColumns );
 view->setSelectionMode(QAbstractItemView::ContiguousSelection);
    view->verticalHeader()->setStyleSheet("QHeaderView::section { "
                                          "border-top: 0px solid lightGrey;"
                                          "border-bottom: 1px solid lightGrey;"
                                          "border-right: 1px solid lightGrey;"
                                          "background:white;"
                                          "}");
    view->horizontalHeader()->setStyleSheet("QHeaderView::section { "
                                            "border-top: 0px solid lightGrey;"
                                            "border-bottom: 1px solid lightGrey;"
                                            "border-right: 1px solid lightGrey;"
                                            "background:white;"
                                            "}");
    view->resizeColumnsToContents();

}

MainWindow::~MainWindow()
{
    delete ui;
    delete view;
}
