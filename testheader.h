#ifndef TESTHEADER_H
#define TESTHEADER_H

#include <QHeaderView>

class TestHeader : public QHeaderView
{
    Q_OBJECT
public:
    TestHeader(Qt::Orientation orientation, QWidget *parent = nullptr);
virtual ~TestHeader();
private slots:
    void onResizeHeader(int index, int o_size, int n_size);
    void calcSize();

signals:
    void headerResized(int index,int o_size,int n_size);

private:
    void mousePressEvent(QMouseEvent *e)override;
    void mouseReleaseEvent(QMouseEvent *e)override;
    void mouseMoveEvent(QMouseEvent *e)override;

    bool isResize;
    int headerHeight;
    int defaultWidth;
    int testMin;
    int headerWidth;
};

#endif // TESTHEADER_H
