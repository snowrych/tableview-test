#include "testmodel.h"
#include <QDebug>
#include <QModelIndex>
#include <QFont>
TestModel::TestModel(QObject *p) : QAbstractTableModel(p),
    m_ori(TestModel::Horizontal)
{

}

int TestModel::rowCount(const QModelIndex &p) const
{
    Q_UNUSED(p)
    return  m_ori == TestModel::Vertical ? 5 : 3;
}

int TestModel::columnCount(const QModelIndex &p) const
{
    Q_UNUSED(p)
    return  m_ori == TestModel::Vertical ? 3 : 5;
}

QVariant TestModel::data(const QModelIndex &index, int role) const
{
    if(role == Qt::DisplayRole) {
        return QVariant(QString("row:%1 col:%2").arg(index.row()).arg(index.column()));
    }

    return QVariant();
}
// headerData
QVariant TestModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    Q_UNUSED(section)
    if(orientation == Qt::Vertical && role == Qt::DisplayRole) {
        return QVariant("Длинная Запись из БД"); // ++  QString::number(section)
    } else if(orientation == Qt::Horizontal && role == Qt::DisplayRole) {
        return QVariant("Классификация нарушения");
    }
    return QVariant();
}

TestModel::Orientation TestModel::ori() const
{
    return m_ori;
}

void TestModel::setOri(const TestModel::Orientation &ori)
{
    m_ori = ori;
}
