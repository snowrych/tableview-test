#ifndef TESTMODEL_H
#define TESTMODEL_H

#include <QAbstractTableModel>
#include <QString>
class TestModel : public QAbstractTableModel
{
public:
    TestModel(QObject *p = nullptr);
    enum Orientation {
        Vertical = 0x1,
        Horizontal = 0x2
    };

    TestModel::Orientation ori() const;
    void setOri(const TestModel::Orientation &ori);

private:
    int rowCount(const QModelIndex &p) const override;
    int columnCount(const QModelIndex &p) const override;
    QVariant data(const QModelIndex &index, int role) const override;
    QVariant headerData(int section, Qt::Orientation orientation, int role) const override;

    TestModel::Orientation m_ori;
};

#endif // TESTMODEL_H
